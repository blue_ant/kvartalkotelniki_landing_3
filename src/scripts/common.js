/*=require ./includes/blocks/*.js*/
/*=require ./includes/chunks/fancybox_defaults.js */

// Privacy Policy Scroll
const ps = new PerfectScrollbar('.PrivacyPolicy');

$('[data-src="#PrivacyPolicyPopup"]').fancybox({
  baseTpl:
    '<div class="fancybox-container FancyboxContainer FancyboxContainer-privacyPolicy" role="dialog" tabindex="-1">' +
    '<div class="fancybox-bg"></div>' +
    '<div class="fancybox-inner">' +
    '<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' +
    '<div class="fancybox-toolbar FancyboxToolbar">{{buttons}}</div>' +
    '<div class="fancybox-navigation">{{arrows}}</div>' +
    '<div class="fancybox-stage"></div>' +
    '<div class="fancybox-caption"></div>' +
    "</div>" +
    "</div>",

  btnTpl: {
    smallBtn:
    	'<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small FancyboxCloseBtn" title="{{CLOSE}}">' + '<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17"><g fill="#5B6573"><path d="M1.485.071l15.557 15.556-1.415 1.415L.071 1.485z"/><path d="M15.627.071l1.415 1.414L1.485 17.042.071 15.627z"/></g></svg>' + "</button>"
  },

  slideClass: "FancyboxSlide",
  
  afterLoad: function () {
  	ps.update();
  }
});