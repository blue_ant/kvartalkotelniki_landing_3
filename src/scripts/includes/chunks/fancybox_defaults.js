$.extend($.fancybox.defaults, {
	hash: false,
	infobar: true,
	buttons: [
		'close'
	],
	margin: [40, 40],
	lang: 'ru',
	transitionEffect: "slide",
	touch: false,
	animationDuration : 200,
	mobile: {
		clickContent: false,
		clickSlide: false,
	},
	i18n: {
		'ru': {
			CLOSE: 'Закрыть',
			NEXT: 'Далее',
			PREV: 'Назад',
			ERROR: 'Не удалось открыть галерею - попробуйте позже',
			PLAY_START: 'Запустить слайдшоу',
			PLAY_STOP: 'Пауза',
			FULL_SCREEN: 'На весь экран',
			THUMBS: 'Миниатуры'
		},
	},

	afterClose: function() {
		if (this.type === "inline") {

			let $form = this.$content.find('form:first');
			let validator = $form.trigger('reset').data("validator");
			if (validator) {
				validator.resetForm();
				$form.find('input.error').removeClass('error');
			}

			let $successText = this.$content.find('.FormSuccess');
			if ($successText.length) {
				$form.show();
				$successText.remove();
			}
		}
	}
});
