// Fullpage plugin
var isfullPage = false;

if ($(window).width() < 1366)
{
	createfullPage();
}

function createfullPage()
{
	if(isfullPage === false)
	{
		isfullPage = true;

		$('#fullpage').fullpage({
			verticalCentered: false,
			scrollOverflow: true
		});
	}
}

$(window).resize(function()
{
	if ($(window).width() < 1366)
	{
		createfullPage();
	} 
	else
	{
		if(isfullPage === true)
		{
			isfullPage = false;
			$.fn.fullpage.destroy('all');
		}
	}
});

// Countdown Timer
var dateTime = $('.Timer_wrapper').data('datetime');

$('.Timer_wrapper').countdown(dateTime, function (event) {
	var hours = parseInt(event.strftime('%-H')),
		minutes = parseInt(event.strftime('%-M')),
		seconds = parseInt(event.strftime('%-S'));

	$('.Timer_item-hours .Timer_digit-tens').text(parseInt(hours / 10));
	$('.Timer_item-hours .Timer_digit-units').text(hours % 10);

	$('.Timer_item-minutes .Timer_digit-tens').text(parseInt(minutes / 10));
	$('.Timer_item-minutes .Timer_digit-units').text(minutes % 10);

	$('.Timer_item-seconds .Timer_digit-tens').text(parseInt(seconds / 10));
	$('.Timer_item-seconds .Timer_digit-units').text(seconds % 10);
});

// Remove timer if the date is in the past
var userDate = new Date(Date.parse(dateTime)),
	now = new Date();

if (now > userDate) {
	$('.Timer').remove();
}